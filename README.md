# CHDatePicker

[![CI Status](http://img.shields.io/travis/coderdc/CHDatePicker.svg?style=flat)](https://travis-ci.org/coderdc/CHDatePicker)
[![Version](https://img.shields.io/cocoapods/v/CHDatePicker.svg?style=flat)](http://cocoapods.org/pods/CHDatePicker)
[![License](https://img.shields.io/cocoapods/l/CHDatePicker.svg?style=flat)](http://cocoapods.org/pods/CHDatePicker)
[![Platform](https://img.shields.io/cocoapods/p/CHDatePicker.svg?style=flat)](http://cocoapods.org/pods/CHDatePicker)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CHDatePicker is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CHDatePicker'
```

## Author

EvilWizard, duanchao19900812@gmail.com

## License

CHDatePicker is available under the MIT license. See the LICENSE file for more info.
