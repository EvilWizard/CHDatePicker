Pod::Spec.new do |s|
  s.name             = 'CHDatePicker'
  s.version          = '0.2.1'
  s.summary          = 'Date picker'

  s.description      = <<-DESC
A date picker contains yyyy.MM.dd yyyy.MM MM.dd.EEEE yyyy
                       DESC

  s.homepage         = 'https://gitlab.com/EvilWizard/CHDatePicker.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'EvilWizard' => 'duanchao19900812@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/EvilWizard/CHDatePicker.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'

  s.source_files = 'CHDatePicker/Classes/**/*'
  s.dependency 'CHDateUtil'
end
