//
//  CHDatePicker.h
//  CHDatePicker
//
//  Created by 行者栖处 on 2018/2/22.
//

#import <UIKit/UIKit.h>

#define CHScreenWidth [UIScreen mainScreen].bounds.size.width
#define CHScreenHeight [UIScreen mainScreen].bounds.size.height
#define CH_SAFE_BLOCK(BlockName, ...) ({ !BlockName ? nil : BlockName(__VA_ARGS__); })
#define CHPIX(i) (i / [UIScreen mainScreen].scale)
#define CHAppKeyWindow [UIApplication sharedApplication].keyWindow

typedef NS_ENUM(NSUInteger, CHDatePickerMode) {
    CHDatePickerModeYearMonthDay                    = 0,// yyyy.MM.dd
    CHDatePickerModeYearMonth                       = 1,// yyyy.MM
    CHDatePickerModeMonthDay                        = 2,// yyyy.MM.dd
    CHDatePickerModeYear                            = 3,// yyyy
    CHDatePickerModeYearMonthDayHourMinute          = 4,// yyyy.MM.dd HH.mm
    CHDatePickerModeYearMonthDayHourMinuteSecond    = 5,// yyyy.MM.dd HH:mm:ss
};

@class CHDatePicker;

@protocol CHDatePickerDelegate <NSObject>

/**
 选中日期确认回调

 @param datePicker 日期选择器
 @param date 选中的日期
 */
- (void)datePicker:(CHDatePicker *)datePicker didSelectedDate:(NSString *)date;

@end

@interface CHDatePicker : UIView

/**
 *  代理
 */
@property (nonatomic, weak) id <CHDatePickerDelegate> delegate;

/**
 *  按钮字体颜色(默认blueColor)
 */
@property (nonatomic, copy) UIColor *buttonFontColor;

/**
 *  选中日期展示的字体颜色(默认orangeColor)
 */
@property (nonatomic, copy) UIColor *contentLabelFontColor;

/**
 *  日期选择器文本字体颜色(默认blackColor)
 */
@property (nonatomic, copy) UIColor *datePickerFontColor;

/**
 *  按钮字体大小(默认13)
 */
@property (nonatomic, assign) CGFloat buttonFontSize;

/**
 *  选中日期展示字体大小(默认13)
 */
@property (nonatomic, assign) CGFloat contentLabelFontSize;

/**
 *  日期选择器文本字体大小(默认13)
 */
@property (nonatomic, assign) CGFloat datePickerFontSize;

/**
 *  是否显示取消,确认按钮 默认为YES
 */
@property (nonatomic, assign) BOOL showActionButton;

/**
 *  datePicker宽度(默认屏宽)
 */
@property (nonatomic, assign) CGFloat datePickerWidth;

/**
 *  datePicker坐标x值(默认0)
 */
@property (nonatomic, assign) CGFloat datePickerX;

/**
 *  日期选择视图依附的view(默认为keyWindow)
 */
@property (nonatomic, strong) UIView *toView;

/**
 初始化一个日期选择器,并展示

 @param mode 日期选择器模式
 @param date 默认日期(日期格式需与模式保持一致)
 @param otherSetting 设置
 @return 日期选择器
 */
+ (instancetype)showWithDatePickerMode:(CHDatePickerMode)mode
                           displayDate:( NSString *)date
                          otherSetting:(void (^) (CHDatePicker *datePicker))otherSetting;

@end
