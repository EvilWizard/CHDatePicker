//
//  CHDatePicker.m
//  CHDatePicker
//
//  Created by 行者栖处 on 2018/2/22.
//

#import "CHDatePicker.h"
#import "CHDateUtil.h"

@interface CHDatePicker ()<UIPickerViewDelegate,UIPickerViewDataSource>

@property (nonatomic, assign) BOOL isSelected;

@property (nonatomic, strong) UIView *backView;

@property (nonatomic, strong) UIPickerView *pickerView;

@property (nonatomic, strong) UIButton *cancelButton;

@property (nonatomic, strong) UIButton *confirmButton;

@property (nonatomic, strong) UILabel *contentLabel;

@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, copy) NSString *date;

@property (nonatomic, assign) CHDatePickerMode datePickerMode;

@property (nonatomic, strong) NSArray *years;

@property (nonatomic, strong) NSArray *months;

@property (nonatomic, strong) NSArray *days;

@property (nonatomic, strong) NSArray *hours;

@property (nonatomic, strong) NSArray *minutes;

@property (nonatomic, strong) NSArray *seconds;

@property (nonatomic, assign) NSInteger yearIndex;

@property (nonatomic, assign) NSInteger monthIndex;

@property (nonatomic, assign) NSInteger dayIndex;

@property (nonatomic, assign) NSInteger hourIndex;

@property (nonatomic, assign) NSInteger minuteIndex;

@property (nonatomic, assign) NSInteger secondIndex;

@property (nonatomic, strong) NSArray <NSArray <NSString *>*>*components;

@property (nonatomic, copy) NSString *selectedDate;

@end

NSString *const kCHDatePickerObsever_YearIndex = @"yearIndex";
NSString *const kCHDatePickerObsever_MonthIndex = @"monthIndex";
NSString *const kCHDatePickerObsever_DayIndex = @"dayIndex";
NSString *const kCHDatePickerObsever_HourIndex = @"hourIndex";
NSString *const kCHDatePickerObsever_MinuteIndex = @"minuteIndex";
NSString *const kCHDatePickerObsever_SecondIndex = @"secondIndex";

@implementation CHDatePicker

+ (instancetype)showWithDatePickerMode:(CHDatePickerMode)mode displayDate:(NSString *)date otherSetting:(void (^)(CHDatePicker *))otherSetting {
    CHDatePicker *datePickerView = [[CHDatePicker alloc] initWithFrame:CGRectMake(0, CHScreenHeight, CHScreenWidth, CHScreenHeight *0.4)];
    CH_SAFE_BLOCK(otherSetting,datePickerView);
    datePickerView.datePickerMode = mode;
    datePickerView.date = date;

    [datePickerView show];
    return datePickerView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setDefaultSettings];
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.cancelButton];
        [self addSubview:self.confirmButton];
        [self addSubview:self.contentLabel];
        [self addSubview:self.lineView];
        [self addSubview:self.pickerView];
        [self addObserver:self forKeyPath:kCHDatePickerObsever_YearIndex options:NSKeyValueObservingOptionNew context:nil];
        [self addObserver:self forKeyPath:kCHDatePickerObsever_MonthIndex options:NSKeyValueObservingOptionNew context:nil];
        [self addObserver:self forKeyPath:kCHDatePickerObsever_DayIndex options:NSKeyValueObservingOptionNew context:nil];
        [self addObserver:self forKeyPath:kCHDatePickerObsever_HourIndex options:NSKeyValueObservingOptionNew context:nil];
        [self addObserver:self forKeyPath:kCHDatePickerObsever_MinuteIndex options:NSKeyValueObservingOptionNew context:nil];
        [self addObserver:self forKeyPath:kCHDatePickerObsever_SecondIndex options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (void)setDefaultSettings {
    self.clipsToBounds = YES;
    self.buttonFontColor = [UIColor colorWithRed:(3/255.0) green:(169/255.0) blue:(244/255.0) alpha:1];
    self.buttonFontSize = 13;
    self.contentLabelFontColor = [UIColor orangeColor];
    self.contentLabelFontSize = 13;
    self.datePickerFontSize = 13;
    self.datePickerFontColor = [UIColor colorWithRed:(255/255.0) green:(149/255.0) blue:(63/255.0) alpha:1];
    self.showActionButton = YES;
}

#pragma mark - UIPickerViewDelegate and UIPickerViewDataSource

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    if (self.datePickerMode == CHDatePickerModeMonthDay) {
        if (component == 0) {
            return CGFLOAT_MIN;
        }
        else {
            return self.bounds.size.width / (self.components.count - 1);
        }
    }
    return (self.bounds.size.width - 20) / self.components.count;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 30;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return self.components.count;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.components[component].count;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    NSString *s = self.components[component][row];
    
    UILabel *label = [[UILabel alloc] init];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor blackColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue" size:CHScreenWidth > 320 ? 13 : 12];
    
    if (component == 2 && self.isSelected && (self.datePickerMode == CHDatePickerModeYearMonthDay || self.datePickerMode == CHDatePickerModeMonthDay || self.datePickerMode == CHDatePickerModeYearMonthDayHourMinute || self.datePickerMode == CHDatePickerModeYearMonthDayHourMinuteSecond)) {
        NSString *year = self.components[0][self.yearIndex];
        NSString *month = self.components[1][self.monthIndex];
        NSString *day = s;
        NSString *dateText = [NSString stringWithFormat:@"%@%@%@",year,month,day];
        NSString *date = nil;
        if (self.datePickerMode == CHDatePickerModeMonthDay) {
            date = [CHDateUtil covertDateString:dateText withSourceFormat:@"yyyy年MM月dd日" toTargetFormat:@"dd日EEEE"];
            label.text = date;
        }
        else {
            date = [CHDateUtil covertDateString:dateText withSourceFormat:@"yyyy年MM月dd日" toTargetFormat:@"dd日"];
            label.text = date;
        }
    }
    else {
        label.text = s;
    }
    return label;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.isSelected = YES;
    if (self.datePickerMode == CHDatePickerModeMonthDay) {// 模式是月日 年月联动
        if (component == 1) {
            if (row <= 11) {
                NSInteger yearComponent = [self.pickerView selectedRowInComponent:0];
                [self.pickerView selectRow:yearComponent - 1 inComponent:0 animated:NO];
                [self.pickerView selectRow:12 + row inComponent:1 animated:NO];
                
            }
            if (row >= 24) {
                NSInteger yearComponent = [self.pickerView selectedRowInComponent:0];
                [self.pickerView selectRow:yearComponent + 1 inComponent:0 animated:NO];
                [self.pickerView selectRow:row - 12 inComponent:1 animated:NO];
            }
            self.monthIndex = row;
        }
        if (component == 2) {
            self.dayIndex = row;
        }
        self.yearIndex = [pickerView selectedRowInComponent:0];
        [self reloadDaysComponentsWithYearIndex:self.yearIndex monthIndex:self.monthIndex];
    }
    
    if (self.datePickerMode == CHDatePickerModeYearMonthDay) {
        if (component == 0) {
            self.yearIndex = row;
            [self reloadDaysComponentsWithYearIndex:self.yearIndex monthIndex:self.monthIndex];
        }
        if (component == 1) {
            self.monthIndex = row;
            [self reloadDaysComponentsWithYearIndex:self.yearIndex monthIndex:self.monthIndex];
        }
        if (component == 2) {
            self.dayIndex = row;
        }
    }
    
    if (self.datePickerMode == CHDatePickerModeYearMonth) {
        if (component == 0) {
            self.yearIndex = row;
        }
        if (component == 1) {
            self.monthIndex = row;
        }
    }
    
    if (self.datePickerMode == CHDatePickerModeYear) {
        if (component == 0) {
            self.yearIndex = row;
        }
    }
    
    if (self.datePickerMode == CHDatePickerModeYearMonthDayHourMinute ||
        self.datePickerMode == CHDatePickerModeYearMonthDayHourMinuteSecond) {
        if (component == 0) {
            self.yearIndex = row;
            [self reloadDaysComponentsWithYearIndex:self.yearIndex monthIndex:self.monthIndex];
        }
        if (component == 1) {
            self.monthIndex = row;
            [self reloadDaysComponentsWithYearIndex:self.yearIndex monthIndex:self.monthIndex];
        }
        if (component == 2) {
            self.dayIndex = row;
        }
        if (component == 3) {
            self.hourIndex = row;
        }
        if (component == 4) {
            self.minuteIndex = row;
        }
        if (component == 5 &&
            self.datePickerMode == CHDatePickerModeYearMonthDayHourMinuteSecond) {
            self.secondIndex = row;
        }
    }
}

#pragma mark - events

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    if (keyPath == kCHDatePickerObsever_YearIndex ||
        keyPath == kCHDatePickerObsever_MonthIndex ||
        keyPath == kCHDatePickerObsever_DayIndex ||
        keyPath == kCHDatePickerObsever_HourIndex ||
        keyPath == kCHDatePickerObsever_MinuteIndex ||
        keyPath == kCHDatePickerObsever_SecondIndex) {
        NSMutableString *s = [NSMutableString string];
        NSString *sourceDateFormat = @"yyyy年MM月dd日";
        NSString *targetDateFormat = @"yyyy.MM.dd";
        
        switch (self.datePickerMode) {
            case CHDatePickerModeYearMonthDay:
            case CHDatePickerModeMonthDay:
            {
                NSString *year = self.components[0][self.yearIndex];
                [s appendString:year];
                NSString *month = self.components[1][self.monthIndex];
                [s appendString:month];
                NSString *day = self.components[2][self.dayIndex];
                [s appendString:day];
                sourceDateFormat = @"yyyy年MM月dd日";
                targetDateFormat = @"yyyy.MM.dd";
            }
                break;
                case CHDatePickerModeYearMonth:
            {
                NSString *year = self.components[0][self.yearIndex];
                [s appendString:year];
                NSString *month = self.components[1][self.monthIndex];
                [s appendString:month];
                sourceDateFormat = @"yyyy年MM月";
                targetDateFormat = @"yyyy.MM";
            }
                break;
                
                case CHDatePickerModeYear:
            {
                NSString *year = self.components[0][self.yearIndex];
                [s appendString:year];
                sourceDateFormat = @"yyyy年";
                targetDateFormat = @"yyyy";
            }
                break;
                
            case CHDatePickerModeYearMonthDayHourMinute:
            {
                NSString *year = self.components[0][self.yearIndex];
                [s appendString:year];
                NSString *month = self.components[1][self.monthIndex];
                [s appendString:month];
                NSString *day = self.components[2][self.dayIndex];
                [s appendString:day];
                [s appendString:@" "];
                NSString *hour = self.components[3][self.hourIndex];
                [s appendString:hour];
                NSString *minute = self.components[4][self.minuteIndex];
                [s appendString:minute];
                sourceDateFormat = @"yyyy年MM月dd日 HH时mm分";
                targetDateFormat = @"yyyy.MM.dd HH:mm";
            }
                break;
                
            case CHDatePickerModeYearMonthDayHourMinuteSecond:
                
            {
                NSString *year = self.components[0][self.yearIndex];
                [s appendString:year];
                NSString *month = self.components[1][self.monthIndex];
                [s appendString:month];
                NSString *day = self.components[2][self.dayIndex];
                [s appendString:day];
                [s appendString:@" "];
                NSString *hour = self.components[3][self.hourIndex];
                [s appendString:hour];
                NSString *minute = self.components[4][self.minuteIndex];
                [s appendString:minute];
                NSString *second = self.components[5][self.secondIndex];
                [s appendString:second];
                sourceDateFormat = @"yyyy年MM月dd日 HH时mm分ss秒";
                targetDateFormat = @"yyyy.MM.dd HH:mm:ss";
            }
                break;
        }
        
        self.selectedDate = [CHDateUtil covertDateString:s withSourceFormat:sourceDateFormat toTargetFormat:targetDateFormat];
        self.contentLabel.text = self.selectedDate;
    }
}

- (void)cancelButtonDidClick {
    [self backViewDidClick];
}

- (void)confirmButtonDidClick {
    [self dismiss];
    if ([self.delegate respondsToSelector:@selector(datePicker:didSelectedDate:)]) {
        [self.delegate datePicker:self didSelectedDate:self.contentLabel.text];
    }
}

- (void)backViewDidClick {
    [self dismiss];
    if (!self.showActionButton && [self.delegate respondsToSelector:@selector(datePicker:didSelectedDate:)]) {
        [self.delegate datePicker:self didSelectedDate:self.contentLabel.text];
    }
    else {
        [self configDefaultWith:self.date];
    }
}

#pragma mark - privates
// 年月日及月日模式下,当选中年和月时
- (void)reloadDaysComponentsWithYearIndex:(NSInteger)yearIndex monthIndex:(NSInteger)monthIndex {
    [self daysWithYear:yearIndex month:monthIndex];
    self.components = [self getComponentsArray];
    [self.pickerView reloadComponent:2];
    if (self.dayIndex > self.days.count - 1) {
        self.dayIndex = self.days.count - 1;
    }
}

- (void)show {
    UIView *toView = self.toView ? self.toView : CHAppKeyWindow;
    if (!self.backView.superview) {
        [toView addSubview:self.backView];
    }
    if (!self.superview) {
        [toView addSubview:self];
        [UIView animateWithDuration:0.25 animations:^{
            CGRect frame = self.frame;
            frame.origin.y = CHScreenHeight *0.6;
            self.frame = frame;
        }];
    }
}

- (void)dismiss {
    [UIView animateWithDuration:0.25 animations:^{
        self.backView.alpha = 0.f;
        CGRect frame = self.frame;
        frame.origin.y = CHScreenHeight;
        self.frame = frame;
    } completion:^(BOOL finished) {
        [self.backView removeFromSuperview];
        [self removeFromSuperview];
    }];
}

- (NSArray *)getComponentsArray {
    
    if (self.datePickerMode == CHDatePickerModeYearMonthDay) {
        return @[self.years,self.months,self.days];
    }
    else if (self.datePickerMode == CHDatePickerModeYearMonth) {
        return @[self.years,self.months];
    }
    else if (self.datePickerMode == CHDatePickerModeMonthDay) {
        return @[self.years,self.months,self.days];
    }
    else if (self.datePickerMode == CHDatePickerModeYear) {
        return @[self.years];
    }
    else if (self.datePickerMode == CHDatePickerModeYearMonthDayHourMinute) {
        return @[self.years,self.months,self.days,self.hours,self.minutes];
    }
    else if (self.datePickerMode == CHDatePickerModeYearMonthDayHourMinuteSecond) {
        return @[self.years,self.months,self.days,self.hours,self.minutes,self.seconds];
    }
    return @[];
}

// 判断是否是闰年
- (BOOL)isLeapYearWithYear:(NSInteger)yearNum month:(NSInteger)monthNum {
    
    if ((yearNum % 4 == 0 && yearNum % 100 != 0) ||
         (yearNum % 100 == 0 && yearNum % 400 == 0)) {
        return YES;
    }
    return NO;
}

- (NSInteger)daysWithYear:(NSInteger)yearNum month:(NSInteger)monthNum {
    yearNum = yearNum + 1990;
    monthNum = monthNum + 1;
    BOOL isLeapYear = [self isLeapYearWithYear:yearNum month:monthNum];
    if (monthNum % 12 == 0 ||
        monthNum % 12 == 1 ||
        monthNum % 12 == 3 ||
        monthNum % 12 == 5 ||
        monthNum % 12 == 7 ||
        monthNum % 12 == 8 ||
        monthNum % 12 == 10 ||
        monthNum % 12 == 12) {
        [self setupDaysWith:31];
        return 31;
    }
    else if (monthNum % 12 == 2) {
        NSInteger dayNum = 0;
        if (isLeapYear) {
            dayNum = 29;
        }
        else {
            dayNum = 28;
        }
        [self setupDaysWith:dayNum];
        return dayNum;
    }
    else {
        [self setupDaysWith:30];
        return 30;
    }
}

- (void)setupYears {
    NSMutableArray *yearsM = [NSMutableArray array];
    for (int i = 1990; i <= 2100; i++) {
        [yearsM addObject:[NSString stringWithFormat:@"%d年",i]];
    }
    self.years = yearsM.copy;
}

- (void)setupMonths {
    NSMutableArray *monthsM = [NSMutableArray array];
    if (self.datePickerMode == CHDatePickerModeMonthDay) {
        for (int i = 0; i < 3; i++) {
            for (int j = 1; j <= 12; j++) {
                [monthsM addObject:[NSString stringWithFormat:@"%02d月",j]];
            }
        }
    }
    else {
        for (int j = 1; j <= 12; j++) {
            [monthsM addObject:[NSString stringWithFormat:@"%02d月",j]];
        }
    }
    self.months = monthsM.copy;
}

- (void)setupDaysWith:(NSInteger)days {
    NSMutableArray *daysM = [NSMutableArray array];
    for (int j = 1; j <= days; j++) {
        [daysM addObject:[NSString stringWithFormat:@"%02d日",j]];
    }
    self.days = daysM.copy;
}

- (void)setupHours {
    NSMutableArray *hoursM = [NSMutableArray array];
    for (int i = 0; i < 24; i ++) {
        [hoursM addObject:[NSString stringWithFormat:@"%02d时",i]];
    }
    self.hours = hoursM.copy;
}

- (void)setupMinutes {
    NSMutableArray *minutesM = [NSMutableArray array];
    for (int i = 0; i < 60; i ++) {
        [minutesM addObject:[NSString stringWithFormat:@"%02d分",i]];
    }
    self.minutes = minutesM.copy;
}

- (void)setupSeconds {
    NSMutableArray *secondsM = [NSMutableArray array];
    for (int i = 0; i < 60; i ++) {
        [secondsM addObject:[NSString stringWithFormat:@"%02d秒",i]];
    }
    self.seconds = secondsM.copy;
}

#pragma mark - getter and setter

- (UIButton *)cancelButton {
    if (!_cancelButton) {
        _cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 64, 44)];
        [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        _cancelButton.titleLabel.font = [UIFont systemFontOfSize:CHScreenWidth > 320 ? self.buttonFontSize : self.buttonFontSize - 1];
        [_cancelButton setTitleColor:self.buttonFontColor forState:UIControlStateNormal];
        [_cancelButton addTarget:self action:@selector(cancelButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelButton;
}

- (UIButton *)confirmButton {
    if (!_confirmButton) {
        _confirmButton = [[UIButton alloc] initWithFrame:CGRectMake(self.bounds.size.width - 64, 0, 64, 44)];
        [_confirmButton setTitle:@"确定" forState:UIControlStateNormal];
        _confirmButton.titleLabel.font = [UIFont systemFontOfSize:CHScreenWidth > 320 ? self.buttonFontSize : self.buttonFontSize - 1];
        [_confirmButton setTitleColor:self.buttonFontColor forState:UIControlStateNormal];
        [_confirmButton addTarget:self action:@selector(confirmButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _confirmButton;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc] init];
        CGFloat left = CGRectGetMaxX(self.cancelButton.frame) + 10;
        _contentLabel.frame = CGRectMake(left, 0, self.bounds.size.width - 2 *left, 44);
        _contentLabel.textAlignment = NSTextAlignmentCenter;
        _contentLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:CHScreenWidth > 320 ? self.contentLabelFontSize : self.contentLabelFontSize - 1];
        _contentLabel.textColor = self.contentLabelFontColor;
    }
    return _contentLabel;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.contentLabel.frame), self.bounds.size.width, CHPIX(1))];
        _lineView.backgroundColor = [UIColor lightGrayColor];
    }
    return _lineView;
}

- (UIPickerView *)pickerView {
    if (!_pickerView) {
        _pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.lineView.frame), self.bounds.size.width, self.bounds.size.height - 44)];
        _pickerView.delegate = self;
        _pickerView.dataSource = self;
        _pickerView.showsSelectionIndicator = YES;
    }
    return _pickerView;
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, CHScreenHeight)];
        _backView.backgroundColor = [UIColor blackColor];
        _backView.alpha = 0.5f;
        UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backViewDidClick)];
        [_backView addGestureRecognizer:tapGes];
    }
    return _backView;
}

- (void)setShowActionButton:(BOOL)showActionButton {
    _showActionButton = showActionButton;
    self.cancelButton.hidden = !showActionButton;
    self.confirmButton.hidden = !showActionButton;
    if (showActionButton) {
        CGFloat left = CGRectGetMaxX(self.cancelButton.frame) + 10;
        self.contentLabel.frame = CGRectMake(left, 0, self.bounds.size.width - 2 *left, 44);
    }
    else {
        self.contentLabel.frame = CGRectMake(10, 0, self.bounds.size.width - 2 *10, 44);
    }
}

- (void)setDatePickerMode:(CHDatePickerMode)datePickerMode {
    _datePickerMode = datePickerMode;
    self.isSelected = NO;
    [self setupYears];
    [self setupMonths];
    [self setupDaysWith:31];
    [self setupHours];
    [self setupMinutes];
    [self setupSeconds];
}

- (void)setDate:(NSString *)date {
    if (!date) {
        date = [CHDateUtil stringFromSourceDate:[NSDate dateWithTimeIntervalSinceNow:3600 *8] targetFormat:@"yyyy.MM.dd"];
    }
    _date = date;
    self.isSelected = YES;
    [self configDefaultWith:date];
}

- (void)setDatePickerWidth:(CGFloat)datePickerWidth {
    _datePickerWidth = datePickerWidth;
    [self adjustWidthWith:self width:datePickerWidth];
    [self adjustWidthWith:self.backView width:datePickerWidth];
    [self adjustWidthWith:self.pickerView width:datePickerWidth];
    [self adjustWidthWith:self.lineView width:datePickerWidth];
    [self adjustWidthWith:self.contentLabel width:(self.bounds.size.width - 2*self.cancelButton.bounds.size.width)];
}

- (void)adjustWidthWith:(UIView *)view width:(CGFloat)width {
    CGRect frame = view.frame;
    frame.size.width = width;
    view.frame = frame;
}

- (void)setDatePickerX:(CGFloat)datePickerX {
    _datePickerX = datePickerX;
    [self adjustXWith:self x:datePickerX];
    [self adjustXWith:self.backView x:datePickerX];
    [self adjustXWith:self.confirmButton x:(self.confirmButton.frame.origin.x - datePickerX)];
}

- (void)adjustXWith:(UIView *)view x:(CGFloat)x {
    CGRect frame = view.frame;
    frame.origin.x = x;
    view.frame = frame;
}

- (void)configDefaultWith:(NSString *)date {
    NSDate *defaultDate = nil;
    NSString *dateFormat = @"";
    NSArray <NSString *>*defaultComponents = @[@""];
    
    if (self.datePickerMode == CHDatePickerModeYearMonthDay ||
        self.datePickerMode == CHDatePickerModeMonthDay) {
        self.components = @[self.years,self.months,self.days];
    }
    else if (self.datePickerMode == CHDatePickerModeYearMonth) {
        self.components = @[self.years,self.months];
    }
    else if (self.datePickerMode == CHDatePickerModeYear) {
        self.components = @[self.years];
    }
    
    if (self.datePickerMode == CHDatePickerModeYearMonthDay ||
        self.datePickerMode == CHDatePickerModeMonthDay) {
        dateFormat = @"yyyy.MM.dd";
        defaultComponents = [CHDateUtil componentsWith:date withSourceFormat:dateFormat];
        self.yearIndex = [self stringByDeleteYearMonthDayWith:defaultComponents[0]].integerValue - 1990;
        self.monthIndex = [self stringByDeleteYearMonthDayWith:defaultComponents[1]].integerValue - 1;
        self.dayIndex = [self stringByDeleteYearMonthDayWith:defaultComponents[2]].integerValue - 1;
        [self daysWithYear:self.yearIndex month:self.monthIndex];
        self.components = @[self.years,self.months,self.days];
    }
    else if (self.datePickerMode == CHDatePickerModeYearMonth) {
        dateFormat = @"yyyy.MM";
        defaultComponents = [CHDateUtil componentsWith:date withSourceFormat:dateFormat];
        self.yearIndex = [self stringByDeleteYearMonthDayWith:defaultComponents[0]].integerValue - 1990;
        self.monthIndex = [self stringByDeleteYearMonthDayWith:defaultComponents[1]].integerValue - 1;
    }
    else if (self.datePickerMode == CHDatePickerModeYear) {
        dateFormat = @"yyyy";
        defaultComponents = [CHDateUtil componentsWith:date withSourceFormat:dateFormat];
        self.yearIndex = [self stringByDeleteYearMonthDayWith:defaultComponents[0]].integerValue - 1990;
        self.components = @[self.years];
    }
    else if (self.datePickerMode == CHDatePickerModeYearMonthDayHourMinute) {
        self.components = @[self.years,self.months,self.days,self.hours,self.minutes];
        dateFormat = @"yyyy.MM.dd HH:mm";
        defaultComponents = [CHDateUtil componentsWith:date withSourceFormat:dateFormat];
        self.yearIndex = [self stringByDeleteYearMonthDayWith:defaultComponents[0]].integerValue - 1990;
        self.monthIndex = [self stringByDeleteYearMonthDayWith:defaultComponents[1]].integerValue - 1;
        self.dayIndex = [self stringByDeleteYearMonthDayWith:defaultComponents[2]].integerValue - 1;
        self.hourIndex = [self stringByDeleteYearMonthDayWith:defaultComponents[3]].integerValue;
        self.minuteIndex = [self stringByDeleteYearMonthDayWith:defaultComponents[4]].integerValue;
    }
    else if (self.datePickerMode == CHDatePickerModeYearMonthDayHourMinuteSecond) {
        self.components = @[self.years,self.months,self.days,self.hours,self.minutes,self.seconds];
        dateFormat = @"yyyy.MM.dd HH:mm:ss";
        defaultComponents = [CHDateUtil componentsWith:date withSourceFormat:dateFormat];
        self.yearIndex = [self stringByDeleteYearMonthDayWith:defaultComponents[0]].integerValue - 1990;
        self.monthIndex = [self stringByDeleteYearMonthDayWith:defaultComponents[1]].integerValue - 1;
        self.dayIndex = [self stringByDeleteYearMonthDayWith:defaultComponents[2]].integerValue - 1;
        self.hourIndex = [self stringByDeleteYearMonthDayWith:defaultComponents[3]].integerValue;
        self.minuteIndex = [self stringByDeleteYearMonthDayWith:defaultComponents[4]].integerValue;
        self.secondIndex = [self stringByDeleteYearMonthDayWith:defaultComponents[5]].integerValue;
    }
    defaultDate = [[CHDateUtil dateFromSource:date withSourceFormat:dateFormat] dateByAddingTimeInterval:3600 *8];  
    
    if (!defaultDate) {
        NSString *exception = [NSString stringWithFormat:@"传入日期%@格式与设置模式%@不符",date,dateFormat];
        NSAssert(defaultDate != nil, exception);
    }
    self.contentLabel.text = date;
    [self reloadAllComponents];
}

// 去除字符串中的 '年' '月' '日' '时' '分' '秒'
- (NSString *)stringByDeleteYearMonthDayWith:(NSString *)s {
    NSArray *array = @[@"年",@"月",@"日",@"时",@"分",@"秒"];
    for (int i = 0; i < array.count; i++) {
        NSString *s1 = array[i];
        if ([s containsString:s1]) {
            s = [s stringByReplacingOccurrencesOfString:s1 withString:@""];
        }
    }
    return s;
}

- (void)reloadAllComponents {
    [self.pickerView reloadAllComponents];
    if (self.datePickerMode == CHDatePickerModeYearMonthDay ||
        self.datePickerMode == CHDatePickerModeMonthDay) {
        [self.pickerView selectRow:self.yearIndex inComponent:0 animated:NO];
        if (self.datePickerMode == CHDatePickerModeMonthDay) {
            self.monthIndex = self.monthIndex + 12;
        }
        [self.pickerView selectRow:self.monthIndex inComponent:1 animated:NO];
        [self.pickerView selectRow:self.dayIndex inComponent:2 animated:NO];
    }
    else if (self.datePickerMode == CHDatePickerModeYearMonth) {
        [self.pickerView selectRow:self.yearIndex inComponent:0 animated:NO];
        [self.pickerView selectRow:self.monthIndex inComponent:1 animated:NO];
    }
    else if (self.datePickerMode == CHDatePickerModeYear) {
        [self.pickerView selectRow:self.yearIndex inComponent:0 animated:NO];
    }
    else if (self.datePickerMode == CHDatePickerModeYearMonthDayHourMinute ||
             self.datePickerMode == CHDatePickerModeYearMonthDayHourMinuteSecond) {
        [self.pickerView selectRow:self.yearIndex inComponent:0 animated:NO];
        [self.pickerView selectRow:self.monthIndex inComponent:1 animated:NO];
        [self.pickerView selectRow:self.dayIndex inComponent:2 animated:NO];
        [self.pickerView selectRow:self.hourIndex inComponent:3 animated:NO];
        [self.pickerView selectRow:self.minuteIndex inComponent:4 animated:NO];
        if (self.datePickerMode == CHDatePickerModeYearMonthDayHourMinuteSecond) {
            [self.pickerView selectRow:self.secondIndex inComponent:5 animated:NO];
        }
    }
}

#pragma mark - system method
- (void)dealloc {
    [self removeObserver:self forKeyPath:kCHDatePickerObsever_YearIndex];
    [self removeObserver:self forKeyPath:kCHDatePickerObsever_MonthIndex];
    [self removeObserver:self forKeyPath:kCHDatePickerObsever_DayIndex];
    [self removeObserver:self forKeyPath:kCHDatePickerObsever_HourIndex];
    [self removeObserver:self forKeyPath:kCHDatePickerObsever_MinuteIndex];
    [self removeObserver:self forKeyPath:kCHDatePickerObsever_SecondIndex];
}

@end
