//
//  CHViewController.m
//  CHDatePicker
//
//  Created by coderdc on 02/09/2018.
//  Copyright (c) 2018 coderdc. All rights reserved.
//

#import "CHViewController.h"

#import "CHDatePicker.h"

@interface CHViewController ()<CHDatePickerDelegate>

@property (nonatomic, strong) CHDatePicker *datePicker;

@end

@implementation CHViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addButtons];
	// Do any additional setup after loading the view, typically from a nib.
}

#pragma mark - CHDatePickerDelegate
- (void)datePicker:(CHDatePicker *)datePicker didSelectedDate:(NSString *)date {
    NSLog(@"选中了:%@",date);
}

- (void)btnDidClick:(UIButton *)btn {
    
    NSInteger i = btn.tag - 1000;
    CHDatePickerMode mode = CHDatePickerModeYearMonthDay;
    NSString *s = @"";
    if (i == 0) {
        mode = CHDatePickerModeYearMonthDayHourMinute;
        s = @"2018.01.01 21:10";
    }else if (i == 1) {
        mode = CHDatePickerModeYearMonth;
        s = @"2018.01";
    }else if (i == 2) {
        mode = CHDatePickerModeMonthDay;
        s = @"2018.01.01";
    }
    else if (i == 3) {
        mode = CHDatePickerModeYearMonthDayHourMinuteSecond;
        s = @"2018.01.01 21:10:10";
    }
    
    
    [CHDatePicker showWithDatePickerMode:mode displayDate:s otherSetting:^(CHDatePicker *datePicker) {
        datePicker.delegate = self;
        // 自定义设置
//        datePicker.datePickerWidth = CHScreenWidth;
//        datePicker.datePickerX = CHScreenWidth *0.2;
        datePicker.showActionButton = YES;
        datePicker.toView = self.view;
    }];
    
    
}

- (void)addButtons {
    NSArray *titles = @[@"yyyy.MM.dd HH:mm",@"yyMM",@"MMdd",@"yyyy.MM.dd HH:mm:ss"];
    for (int i = 0; i < titles.count; i++) {
        CGFloat x = 10 + i *80;
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(x, 100, 100, 44)];
        [btn setTitle:titles[i] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.tag = 1000 + i;
        [btn addTarget:self action:@selector(btnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
    }
}
    
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
