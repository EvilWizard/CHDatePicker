//
//  CHDateUtil.h
//  CHDateUtil
//
//  Created by 行者栖处 on 2017/11/8.
//  Copyright © 2017年 dc. All rights reserved.

/// 1. 获取日期格式化器 dateFormatWith:
/// 2. 将日期字符串转化为NSDate类型 dateFromSource: withSourceFormat:
/// 3. 将NSDate类型指定格式化器转化为日期字符串 stringFromSourceDate: targetFormat:
/// 4. 将源日期字符串转化为指定格式的目标日期字符串 covertDateString: withSourceFormat: toTargetFormat:

#import <Foundation/Foundation.h>

@interface CHDateUtil : NSObject

/**
 返回一个日期格式化器
 
 @param format 日期格式化样式
 @return 日期格式化器
 */
+ (NSDateFormatter *)dateFormatWith:(NSString *)format;

/**
 将目标日期字符串转化为NSDate类型
 
 @param string 目标日期字符串
 @param sourceFormat 目标日期字符串的格式化器字符串
 @return 目标日期字符串的NSDate
 */
+ (NSDate *)dateFromSource:(NSString *)string
          withSourceFormat:(NSString *)sourceFormat;

/**
 将源日期转化为目标格式化器样式字符串
 
 @param sourceDate 源日期
 @param targetFormat 目标格式化器字符串
 @return 目标字符串
 */
+ (NSString *)stringFromSourceDate:(NSDate *)sourceDate
                      targetFormat:(NSString *)targetFormat;

/**
 将源日期字符串转化为目标格式化器形式的日期字符串
 
 @param sourceString 源日期字符串
 @param sourceFormat 源格式化器字符串
 @param targetFormat 目标格式化器字符串
 @return 目标字符串
 */
+ (NSString *)covertDateString:(NSString *)sourceString
              withSourceFormat:(NSString *)sourceFormat
                toTargetFormat:(NSString *)targetFormat;

/**
 将来源日期字符串拆分成日期字符串数组([2018年,01月,01日])
 
 @param sourceString 源日期字符串
 @param format 源日期字符串格式化器字符串
 @return 目标数组
 */
+ (NSArray *)componentsWith:(NSString *)sourceString
           withSourceFormat:(NSString *)format;

#pragma mark - 日
/**
 获取今天的日期
 
 @param targetFormat 日期格式化
 @return 今天日期
 */
+ (NSString *)todayDateWithFormat:(NSString *)targetFormat;

/**
 获取昨天的日期
 
 @param targetFormat 日期格式化
 @return 昨天日期字符串
 */
+ (NSString *)yesterdayDateWithFormat:(NSString *)targetFormat;

/**
 获取明天的日期
 
 @param targetFormat 日期格式化
 @return 明天日期字符串
 */
+ (NSString *)tomorrowDateWithFormat:(NSString *)targetFormat;

#pragma mark - 周
/**
 获取指定日期所在周日期区间(周一至周日)
 
 @param dateStr 日期字符串
 @param sourceFormat dateStr日期格式化器
 @param targetFormat 目标日期格式化器
 @return 周日期时间
 */
+ (NSString *)weekdayWithDate:(NSString *)dateStr
                 sourceFormat:(NSString *)sourceFormat
                 targetFormat:(NSString *)targetFormat;

/**
 当前周日期(以'-'连接)
 
 @param format 格式化器
 @return 当前周日期区间
 */
+ (NSString *)currentWeekWithFormat:(NSString *)format;

/**
 上周日期(以'-'连接)
 
 @param format 格式化器
 @return 上周日期区间
 */
+ (NSString *)lastWeekWithFormat:(NSString *)format;

/**
 下周日期(以'-'连接)
 
 @param format 格式化器
 @return 下周日期区间
 */
+ (NSString *)nextWeekWithFormat:(NSString *)format;

#pragma mark - 月
/**
 根据月份字符串获取当月起始及结束日期[开始日期,结束日期]
 
 @param monthStr 月份字符串(yyyy.MM/yyyyMM/yyyyM)
 @param sourceFormat monthStr源格式化器
 @param targetFormat 目标格式化器
 @return 日期数组
 */
+ (NSArray <NSString *>*)monthDateIntervalWithMonth:(NSString *)monthStr
                                       sourceFormat:(NSString *)sourceFormat
                                       targetFormat:(NSString *)targetFormat;

/**
 根据日期字符串获取所在月份
 
 @param day 日期字符串
 @param sourceFormat 元日期字符串格式化器
 @param targetFormat 目标格式化器
 @return 月份
 */
+ (NSString *)monthWithDay:(NSString *)day sourceFormat:(NSString *)sourceFormat targetFormat:(NSString *)targetFormat;

/**
 获取当前月份
 
 @param format 格式化器
 @return 月份
 */
+ (NSString *)currentMonthWithFormat:(NSString *)format;

/**
 获取上个月月份
 
 @param format 格式化器
 @return 月份
 */
+ (NSString *)lastMonthWithFormat:(NSString *)format;

/**
 获取下个月月份
 
 @param format 格式化器
 @return 月份
 */
+ (NSString *)nextMonthWithFormat:(NSString *)format;

#pragma mark - 时间间隔计算
/**
 获取间隔月份的日期
 
 @param interval 间隔月数(可传+-)
 @param sourceDate 源日期
 @param sourceFormat 源日期格式化器
 @param targetFormat 目标日期格式化器
 @return 日期字符串
 */
+ (NSString *)monthIntervalWith:(NSInteger)interval
                     sourceDate:(NSString *)sourceDate
                   sourceformat:(NSString *)sourceFormat
                   targetFormat:(NSString *)targetFormat;

/**
 获取间隔年月日的日期
 
 @param year 间隔的年数
 @param month 间隔的月数
 @param day 间隔的天数
 @param sourceDate 源日期字符串
 @param sourceFormat 原日期字符串格式化器
 @param targetFormat 目标日期格式化器
 @return 日期字符串
 */
+ (NSString *)timeIntervalWithYear:(NSInteger)year
                             month:(NSInteger)month
                               day:(NSInteger)day
                        sourceDate:(NSString *)sourceDate
                      sourceFormat:(NSString *)sourceFormat
                      targetFormat:(NSString *)targetFormat;

/**
 通过NSDateComponents类获取指定日期时间间隔的日期
 
 @param comps 指定日期时间间隔实例
 @param sourceDate 源日期字符串
 @param sourceFormat 源日期格式化器
 @param targetFormat 目标日期格式化器
 @return 日期字符串
 */
+ (NSString *)timeIntervalWithComponents:(NSDateComponents *)comps
                              sourceDate:(NSString *)sourceDate
                            sourceFormat:(NSString *)sourceFormat
                            targetFormat:(NSString *)targetFormat;

#pragma mark - 日期比较
/**
 比较两个日期字符串
 
 @param dateStr1 日期字符串1
 @param dateStr2 日期字符串2
 @param format 格式化字符串
 @return 比较结果
 */
+ (NSComparisonResult)dateStr1:(NSString *)dateStr1 compareDateStr2:(NSString *)dateStr2 sourceFormat:(NSString *)format;

#pragma mark - 通过年份及第几周获取日期
/**
 传入某一年的第几周获取周日期区间
 
 @param year 年份
 @param weekIndex 第几周
 @param targetFormat 格式化器
 @return 结果
 */
+ (NSArray <NSString *>*)weekDayWithYear:(NSInteger)year weekIndex:(NSInteger)weekIndex targetFormat:(NSString *)targetFormat;

#pragma mark - 秒转化为日时分秒

/**
 传入秒返回NSDateComponent对象 （只支持 天 时 分 秒 ）
 
 @param seconds 秒数
 @return NSDateComponent对象
 */
+ (NSDateComponents *)componentsWithSeconds:(NSTimeInterval)seconds;

/**
 传入秒返回时间字符传 HH：mm：ss
 
 @param seconds 秒
 @return HH:mm:ss
 */
+ (NSString *)timeIntervalWithSeconds:(NSTimeInterval)seconds;

@end

